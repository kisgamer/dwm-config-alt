# alternative dwm config
This repo is for my dwm dotfiles. This version is using the ```tilegaps``` patch instead of the ```uselessgaps``` patch, this means there are always gaps around a window.
## Things needed
- JetBrains Mono font family: https://download.jetbrains.com/fonts/JetBrainsMono-2.304.zip
- autostart.sh file in ~/.local/dwm/
- libim2: arch: ```# pacman -S libim2```
- There are more requirements, see: https://gitlab.com/kisgamer/xdg-autostart
- kitty
